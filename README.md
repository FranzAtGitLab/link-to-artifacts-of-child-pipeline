# Link to Artifacts of Child Pipeline

This is a mini project associated with a question about links to artifacts
created by pipelines. The question was posted on the GitLab forums. The thread
can be found here: https://forum.gitlab.com/t/link-to-artifacts-of-child-pipeline/39666

The ci-pipeline of the repository creates two .txt-files. The first one
(`hello.txt`) is directly created by a job specified in the `.gitlab-ci.yml`.
It can be linked to by the URL:

https://gitlab.com/FranzAtGitLab/link-to-artifacts-of-child-pipeline/-/jobs/artifacts/master/raw/hello.txt?job=hello_job

The second one (`world.txt`) is created by a job of a child pipeline.
It could not be linked to by the URL:

https://gitlab.com/FranzAtGitLab/link-to-artifacts-of-child-pipeline/-/jobs/artifacts/master/raw/world.txt?job=world_job

This was fixed in GitLab version 13.5.

For more information see the ci-configuration of this repository and/or the
above mentioned post in the GitLab forums.
